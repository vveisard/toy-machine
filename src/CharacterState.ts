// lib
import { Region, State } from "@steelbreeze/state";
import { BaseNode } from "blueshell";
// clash | common-gam
import { CharacterBlackboard } from "./CharacterBlackboard";
import { UpdateCharacterBehaviorEvent } from "./UpdateCharacterBehaviorEvent";

/**
 * State for a character, in a finite state machine.
 */
export class CharacterState extends State {

  //#region Properties

  /**
   * Behavior tree to be executed while this state is active.
   */
  public get behavior(): BaseNode<CharacterBlackboard, UpdateCharacterBehaviorEvent> {
    return this._behavior;
  }

  //#endregion

  constructor(name: string, private _behavior: BaseNode<CharacterBlackboard, UpdateCharacterBehaviorEvent>, parent?: State | Region | undefined) {
    super(name, parent);
  }
}